const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

module.exports.addProduct = (prod) => {

	let newProduct = new Product({
		image: prod.image,
		name: prod.name,
		description: prod.description,
		price: prod.price
	});

	return newProduct.save().then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return true;
		}
	})
};

module.exports.getAll = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.availableProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqbody) => {
	return Product.findOne({productsId: reqbody.productsId}).then((result, err) => {
		if(err) {
			return err;
		} else {
			return result;
		}
	})
}

module.exports.updateProduct = (reqbody, prod) => {

	let newProduct = {
		image: prod.image,
		name : prod.name,
		description : prod.description,
		price: prod.price
	}

	return Product.findOneAndUpdate({productsId : reqbody.productsId}, newProduct).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveProduct = (paramsId, prod) => {

	return Product.findByIdAndUpdate(paramsId.prodId, {isActive : prod.isActive}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.deleteProduct = (reqbody) => {
	return Product.findOneAndDelete({productsId : reqbody.productsId}).then((result, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}