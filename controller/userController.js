const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		name: requestBody.name,
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})
	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.checkIfEmailExists = (requestBody) => {
	return User.find({email: requestBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
};

module.exports.allUsers = () => {
	return User.find({}).then(result => {
		let index = 0;
		result.forEach(i => {
			result[index].password = "HIDDEN";
			index++;
		})
		return result;
	})
}

module.exports.authenticateUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.checkout = async (data, reqbody) => {
	let increment;
	let name = await Product.findOne({productsId: reqbody.productsId}).then(result => {
		if (result.userOrdered.length == 0) {
			increment = 1;
		} else {
			increment = result.userOrdered.length + 1;
		}

		let userOrder = { 
				userId: reqbody.userId,
				orderId: increment,
				email : data.email
			}

		result.userOrdered.push(userOrder);
		result.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
		return result.name;
	});

	let order = {
		products : [{
			orderId: increment,
			productsId : reqbody.productsId,
			description: reqbody.description,
			productName : name,
			price: reqbody.price,
			quantity : reqbody.quantity
		}],
		totalAmount : reqbody.quantity * reqbody.price
	}

	return await User.findById(reqbody.userId).then(result => {
		result.orderedProduct.push(order);
		return result.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
	})
};

module.exports.getOrder = (reqbody) => {
	let order = User.findOne({name: reqbody.name}).then(result => {
		let increment;
		if (result.orderedProduct.length == 0) {
			increment = 1;
		} else {
			increment = result.orderedProduct.length + 1;
		}

		let userOrder = {
			Orders : []
		}
		let orders = result.orderedProduct.forEach(i => {
			let hours = i.purchaseOn.getHours();
			let midday = hours >= 12 ? 'pm' : 'am';
			hours = (hours % 12) || 12;
			let month = i.purchaseOn.getMonth()+1;
			let day = i.purchaseOn.getUTCDate();
			let year = i.purchaseOn.getUTCFullYear();
			let date = month + "-" + day + "-" + year;
			let time = hours + ":" + i.purchaseOn.getMinutes() + " " + midday;
			let purchaseOn = date + " " + time;
			let prod = {
				orderId: increment,
				productsId: i.products[0].productsId,
				Product : i.products[0].productName,
				quantity : i.products[0].quantity,
				price: i.products[0].price,
				Total : i.totalAmount,
				purchaseOn : purchaseOn,
			}
			userOrder.Orders.push(prod);
		})
		if (result.isAdmin) {
			return false;
		} else {
			return userOrder;
		}
	})
	if (order == false) {
		return false;
	} else {
		return order;
	}
}

module.exports.setAdmin = (reqbody) => {
	return User.findOneAndUpdate(reqbody, {isAdmin: true}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});

};



