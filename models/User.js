const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProduct: [
	{
		products: [{
			orderId: {
				type: Number,
			},
			productsId: {
				type: Number,
			},
			productName: {
				type: String
			},
			description: {
				type: String
			},
			price: {
				type: Number
			},
			quantity: {
				type: Number
			}
		}],
		totalAmount: {
			type: Number
		},
		purchaseOn: {
			type: Date,
			default: new Date()
		}
	}]
});

module.exports = mongoose.model("User", userSchema);