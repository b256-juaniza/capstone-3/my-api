const express = require("express");
const router = express.Router();
const prodController = require("../controller/prodController.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		prodController.addProduct(data.product).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

router.get("/allproducts", (req, res) => {
	prodController.getAll().then(resultFromController => res.send(resultFromController));
})

router.get("/active", (req, res) => {
	prodController.availableProducts().then(resultFromController => res.send(resultFromController));
})

router.post("/check", (req, res) => {
	prodController.getProduct(req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/update", auth.verify, (req, res) => {
	const data = {
		prod: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {
		prodController.updateProduct(req.body, data.prod).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

router.patch("/archive/:prodId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if (data.isAdmin) {
		prodController.archiveProduct(data.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

})

router.delete("/delete", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}

	if (data.isAdmin) {
		prodController.deleteProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

module.exports = router;